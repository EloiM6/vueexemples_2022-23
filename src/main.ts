import "bootstrap/dist/css/bootstrap.min.css"
import { createApp } from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from "./router/router";


//createApp(App).mount('#app')
createApp(App).use(router).mount('#app')
