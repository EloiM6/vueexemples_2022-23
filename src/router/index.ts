import {createRouter, createWebHistory, RouteRecordRaw} from 'vue-router';

const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        name: 'Inici',
        component: () => import('@/components/prova1.vue'),
    },
    {
        path: '/home',
        name: 'home',
        component: () => import('@/components/provaPare.vue'),
    },
    {
        path: '/about',
        name: 'AbouT',
        component: () => import('@/views/about.vue'),
    },
    {
        path: '/param/:id',
        name: 'provaParam',
        component: () => import('@/components/provaParam.vue'),
        props:true
    },
    {
        path: '/cicle/:id',
        name: 'provaRutaCicle',
        component: () => import('@/components/provaRutacicle.vue'),
    },
];
export default routes;
